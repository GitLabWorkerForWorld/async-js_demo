/**
 * Demo async library's every function(include every, everyLimit, everySeries)
 * 1. every
 * 2. everyLimit
 * 3. everySeries
 *
 * @public
 * @param {Function} log The function show log message to depend on other method.
 * @returns none.
 */
function async_every(log) {
    log('Example 4 ================================');
    /**
     * every函數是對同一集合所有元素執行相同的非同步操作。
     * 本方法包含三種執行方式：
     * 1. 集合中所有元素同時執行。
     * 2. 集合中元素按照順序執行。
     * 3. 分為批次執行，批次與批次之間有序。
     *
     * 如果集合中每個元素都符合條件，最終結果回傳true，否則為false。
     */
    var arr = [{
            value: 1,
            delay: 1000 //拉長延遲時間，可模擬長時間的作業程序
        },
        {
            value: 2,
            delay: 2000
        }, {
            value: 3,
            delay: 3000
        }, {
            value: 4,
            delay: 4000
        }, {
            value: 5,
            delay: 5000
        }
    ];
    //
    var add = function(n, callback, timeout) {
        //將參數n遞增1後透過async's callback回傳
        timeout = timeout || 5000;
        setTimeout(function() {
            callback(null, n + 1);
        }, timeout);
    };
    /**。
     * 並行處理，並且利用add function模擬長時間的作業程序，
     * 如果集合中每個元素都符合條件，最終結果回傳true，否則為false。
     */
    //every(coll, iteratee, callback)
    async.every(arr, function(item, callback) {
        log(' enter: ', item.value);
        add(item.value, function(err, n) {
            log('every handle: ', item.value);
            callback(null, (n % 1) === 0);
        }, item.delay);
    }, function(err, result) {
        log('every result: ', result);
    });
    /**。
     * 並行處理，並且利用add function模擬長時間的作業程序，
     * 如果集合中每個元素都符合條件，最終結果回傳true，否則為false。
     * 與every不同之處在於可限制每次並行執行的數量。
     */
    //everyLimit(coll, limit, iteratee, callback)
    async.everyLimit(arr, 2, function(item, callback) {
        log('everyLimit enter: ', item.value);
        add(item.value, function(err, n) {
            log('everyLimit handle: ', item.value);
            callback(null, (n % 1) === 0);
        }, item.delay);
    }, function(err, result) {
        log('everyLimit result: ', result);
    });
    /**
     * 按照陣列元素的順序執行非同步操作，一個程序完成後才進行下一個程序(依序)。
     * 如果集合中每個元素都符合條件，最終結果回傳true，否則為false。
     */
    //everySeries(arr, iterator, callback)
    async.everySeries(arr, function(item, callback) {
        log('everySeries enter: ', item.value);
        add(item.value, function(err, n) {
            log('everySeries handle: ', item.value);
            //callback(false);
            callback(null, item.value > 2);
        }, item.delay);
    }, function(err, result) {
        log('everySeries result: ', result);
    });
    /* console.log explain:
    Example 4 ================================
    async_every.js:54  enter:  1
    async_every.js:54  enter:  2
    async_every.js:54  enter:  3
    async_every.js:54  enter:  4
    async_every.js:54  enter:  5
    async_every.js:69 everyLimit enter:  1
    async_every.js:69 everyLimit enter:  2
    async_every.js:83 everySeries enter:  1
    =============================================
    以上是初次執行的狀況，可以看到every立即執行。
    everyLimit則是兩個線程同時並行。
    everySeries則只有一項線程在運行。
    =============================================
    async_every.js:56 every handle:  1
    async_every.js:60 every result:  true
    async_every.js:71 everyLimit handle:  1
    async_every.js:75 everyLimit result:  true
    async_every.js:85 everySeries handle:  1
    async_every.js:89 everySeries result:  true
    async_every.js:56 every handle:  2
    async_every.js:71 everyLimit handle:  2
    async_every.js:56 every handle:  3
    async_every.js:56 every handle:  4
    async_every.js:56 every handle:  5
    */
} //End of async_every()
