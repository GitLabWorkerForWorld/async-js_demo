/**
 * Demo async library's each function(include each, eachLimit, eachSeries)
 * 1. each
 * 2. eachLimit
 * 3. eachSeries
 *
 * @public
 * @param {Function} log The function show log message to depend on other method.
 * @returns none.
 */
function async_each(log) {
    log('Example 3 ================================');
    /**
     * each函數是對同一集合所有元素執行相同的非同步操作。
     * 本方法包含三種執行方式：
     * 1. 集合中所有元素同時執行。
     * 2. 集合中元素按照順序執行。
     * 3. 分為批次執行，批次與批次之間有序。
     *
     * 如有任何程序出現錯誤，則將錯誤資訊給callback處理，其餘程序則繼續執行，未啟動則忽略。
     */
    var arr = [{
            value: 1,
            delay: 1000 //拉長延遲時間，可模擬長時間的作業程序
        },
        {
            value: 2,
            delay: 2000
        }, {
            value: 3,
            delay: 3000
        }, {
            value: 4,
            delay: 4000
        }, {
            value: 5,
            delay: 5000
        }
    ];
    //
    var add = function(n, callback, timeout) {
        //將參數n遞增1後透過async's callback回傳
        timeout = timeout || 5000;
        setTimeout(function() {
            callback(null, n + 1);
        }, timeout);
    };
    /**。
     * 並行處理，並且利用add function模擬長時間的作業程序，
     * 當發生錯誤時回傳(callback)，但不影響其他執行序。
     */
    //each(coll, iteratee, callback)
    async.each(arr, function(item, callback) {
        log(' enter: ', item.value);
        add(item.value, function(err, n) {
            log('each handle: ', item.value);
            callback('err');
        }, item.delay);
    }, function(err) {
        log('each result: ', err);
    });
    /**。
     * 並行處理，並且利用add function模擬長時間的作業程序，
     * 當發生錯誤時回傳(callback)，但不影響其他執行序。
     * 與each不同之處在於可限制每次並行執行的數量。
     */
    //eachLimit(coll, limit, iteratee, callback)
    async.eachLimit(arr, 2, function(item, callback) {
        log('eachLimit enter: ', item.value);
        add(item.value, function(err, n) {
            log('eachLimit handle: ', item.value);
            callback('err');
        }, item.delay);
    }, function(err) {
        log('eachLimit result: ', err);
    });
    /**
     * 按照陣列元素的順序執行非同步操作，一個程序完成後才進行下一個程序(依序)。
     * 當發生錯誤時回傳(callback)，但不影響其他執行序。
     */
    //eachSeries(arr, iterator, callback)
    async.eachSeries(arr, function(item, callback) {
        log('eachSeries enter: ', item.value);
        add(item.value, function(err, n) {
            log('eachSeries handle: ', item.value);
            callback('err');
        }, item.delay);
    }, function(err) {
        log('eachSeries result: ', err);
    });
    /* console.log explain:
    Example 3 ================================
    async_each.js:54  enter:  1
    async_each.js:54  enter:  2
    async_each.js:54  enter:  3
    async_each.js:54  enter:  4
    async_each.js:54  enter:  5
    async_each.js:69 eachLimit enter:  1
    async_each.js:69 eachLimit enter:  2
    async_each.js:83 eachSeries enter:  1
    =============================================
    以上是初次執行的狀況，可以看到each立即執行。
    eachLimit則是兩個線程同時並行。
    eachSeries則只有一項線程在運行。
    =============================================
    async_each.js:56 each handle:  1
    async_each.js:60 each result:  true
    async_each.js:71 eachLimit handle:  1
    async_each.js:75 eachLimit result:  true
    async_each.js:85 eachSeries handle:  1
    async_each.js:89 eachSeries result:  true
    async_each.js:56 each handle:  2
    async_each.js:71 eachLimit handle:  2
    async_each.js:56 each handle:  3
    async_each.js:56 each handle:  4
    async_each.js:56 each handle:  5
    */
} //End of async_each()
