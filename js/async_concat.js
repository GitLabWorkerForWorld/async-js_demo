/**
 * Demo async library's concat function(include concat, concat's error handler
 * and concatSeries)
 * 1. concat
 * 2. concat's error handler
 * 3. concatSeries
 *
 * @public
 * @param {Function} log The function show log message to depend on other method.
 * @returns none.
 */
function async_concat(log) {
    log('Example 1 ================================');
    /**
     * 將所有非同步操作的結果彙整為一個陣列(concat)
     */
    //concat(arr, iterator(item,callback(err,result)), callback(err,result))
    var data = {
        a: [11,22,33],
        b: [44,55],
        c: 66
    };
    //Keys object, include each delay seconds
    var keys = [
        {name: 'a', delay: 3000},
        {name: 'b', delay: 1000},
        {name: 'c', delay: 2000}
    ];
    /**。
     * 對集合中所有元素進行非同步操作(按照順序)，
     * 並將結果合併為一個陣列，最後彙整至callback function。
     */
    async.concat(keys, function(key,callback) {
        setTimeout(function() {
            callback(null, data[key.name]);
        }, key.delay);
    }, function(err, values) {
        log('concat err: ', err);
        log('concat values: ', values);
    });
    //concat err: null
    //concat values: [ 44, 55, 66, 11, 22, 33 ]
    /**
     * 對集合中所有元素進行非同步操作(按照順序)，
     * 並將結果合併為一個陣列，最後彙整至callback function。
     * 本案例新增錯誤資訊回傳，若執行中途發生錯誤，則直接中斷所有程序並且回傳現有結果。
     */
    async.concat(keys, function(key,callback) {
        setTimeout(function() {
            if(key.name === 'c') {
                callback('errormsg');
            } else {
                callback(null, data[key.name]);
            }//fi
        }, key.delay);
    }, function(err, values) {
        log('concat\' err: ', err);
        log('concat\'err values: ', values);
    });
    //concat's err: errormsg
    //concat's err values: [ 44, 55 ]
    /**
     * 按照陣列元素的順序執行非同步操作，一個程序完成後才進行下一個程序(依序)。
     * 所有結果會彙整成一個陣列傳給callback。
     */
    //concatSeries(arr, iterator, callback)
    async.concatSeries(keys, function(key,callback) {
        setTimeout(function() {
            callback(null, data[key.name]);
        }, key.delay);
    }, function(err, values) {
        log('concatSeries err: ', err);
        log('concatSeries values: ', values);
        log('End of Example 1 =========================');
    });
    //concatSeries err: null
    //concatSeries values: [ 11, 22, 33, 44, 55, 66 ]
}//End of async_concat()
