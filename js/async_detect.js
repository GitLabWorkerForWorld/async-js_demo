/**
 * Demo async library's detect function(include detect, detectLimit, detectSeries)
 * 1. detect
 * 2. detectLimit
 * 3. detectSeries
 *
 * @public
 * @param {Function} log The function show log message to depend on other method.
 * @returns none.
 */
function async_detect(log) {
    log('Example 2 ================================');
    /**
     * 用於取得第一個滿足條件的元素。
     * 有三種執行方式：detect, detectLimit, detectSeries
     * 特色是滿足條件後即回傳，其餘的線程仍然會繼續執行。
     * 但是不會回傳至callback function。
     */
    var arr = [{
            value: 1,
            delay: 1000 //拉長延遲時間，可模擬長時間的作業程序
        },
        {
            value: 2,
            delay: 2000
        }, {
            value: 3,
            delay: 3000
        }, {
            value: 4,
            delay: 4000
        }, {
            value: 5,
            delay: 5000
        }
    ];
    //
    var add = function(n, callback, timeout) {
        //將參數n遞增1後透過async's callback回傳
        timeout = timeout || 5000;
        setTimeout(function() {
            callback(null, n + 1);
        }, timeout);
    };
    /**。
     * 並行處理，並且利用add function模擬長時間的作業程序，
     * 當得到第一個達到條件的結果及回傳(callback)
     */
    //detect(array, iterator(item,callback(test)), callback(result)
    async.detect(arr, function(item, callback) {
        log('detect enter: ', item.value);
        add(item.value, function(err, n) {
            log('detect handle: ', item.value);
            callback((n % 1) === 0); //條件為: n%1 === 0
        }, item.delay);
    }, function(result) {
        log('detect result: ', result);
    });
    /**。
     * 並行處理，並且利用add function模擬長時間的作業程序，
     * 當得到第一個達到條件的結果及回傳(callback)
     * 與detect不同之處在於可限制每次並行執行的數量。
     */
    //detectLimit(coll, limit, iteratee, callback)
    async.detectLimit(arr, 2, function(item, callback) {
        log('detectLimit enter: ', item.value);
        add(item.value, function(err, n) {
            log('detectLimit handle: ', item.value);
            callback((n % 1) === 0); //條件為: n%1 === 0
        }, item.delay);
    }, function(result) {
        log('detectLimit result: ', result);
    });
    /**
     * 按照陣列元素的順序執行非同步操作，一個程序完成後才進行下一個程序(依序)。
     * 當得到第一個達到條件的結果及回傳(callback)
     */
    //detectSeries(arr, iterator, callback)
    async.detectSeries(arr, function(item, callback) {
        log('detectSeries enter: ', item.value);
        add(item.value, function(err, n) {
            log('detectSeries handle: ', item.value);
            callback((n % 1) === 0);
        }, item.delay);
    }, function(result) {
        log('detectSeries result: ', result);
    });
    /* console.log explain:
    Example 2 ================================
    async_detect.js:48 detect enter:  1
    async_detect.js:48 detect enter:  2
    async_detect.js:48 detect enter:  3
    async_detect.js:48 detect enter:  4
    async_detect.js:48 detect enter:  5
    async_detect.js:63 detectLimit enter:  1
    async_detect.js:63 detectLimit enter:  2
    async_detect.js:77 detectSeries enter:  1
    =============================================
    以上是初次執行的狀況，可以看到detect立即執行。
    detectLimit則是兩個線程同時並行。
    detectSeries則只有一項線程在運行。
    =============================================
    async_detect.js:50 detect handle:  1
    async_detect.js:54 detect result:  true
    async_detect.js:65 detectLimit handle:  1
    async_detect.js:69 detectLimit result:  true
    async_detect.js:79 detectSeries handle:  1
    async_detect.js:83 detectSeries result:  true
    async_detect.js:50 detect handle:  2
    async_detect.js:65 detectLimit handle:  2
    async_detect.js:50 detect handle:  3
    async_detect.js:50 detect handle:  4
    async_detect.js:50 detect handle:  5
    */
} //End of async_detect()
