window.onload = function() {
        var log = console.log;
        var funName = 'every';
        switch (funName) {
            case 'concat':
                //Example 1 : concat.
                async_concat(log);
            break;
            case 'detect':
                //Example 2 : detect
                async_detect(log);
            break;
            case 'each':
                //Example 3 : each
                async_each(log);
            break;
            case 'every':
                //Example 4 : every
                async_every(log);
            break;
            default:
                log('nothing');
        } //endswitch
    } //End of window.onload
